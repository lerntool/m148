
# Hier gibt es die nötigen Unterlagen um sich das IoT Home selbst zu bauen

Sie finden eine Vorlage, mit der Sie das Haus aus Sperrholz lasern können.
Sie finden eine Vorlage, mit der Sie das Entwicklerboard selber produzieren können.
Dies ist Ihnen zu aufwändig: Sie können alles auch unter [lerntool.ch](lerntool.ch) bestellen.

![Das IoT Board](https://gitlab.com/lerntool/m148/-/raw/master/IOT%20Home/IoT%20Board/Foto/IoTBoard.png)
![Home1](https://gitlab.com/lerntool/m148/-/raw/master/IOT%20Home/Fotos/Home1.png)
![Home2](https://gitlab.com/lerntool/m148/-/raw/master/IOT%20Home/Fotos/Home2.png)
![Home3](https://gitlab.com/lerntool/m148/-/raw/master/IOT%20Home/Fotos/Home3.png)
![Home4](https://gitlab.com/lerntool/m148/-/raw/master/IOT%20Home/Fotos/Home4.png)
