# Das IoT Board
Das IoT Board dient dazu einfach und schnell IoT Elemente (Sensoren und Aktoren) zu simulieren.

So sind alle Anschlüsse des ESP32 auf Steckverbindungen geführt, damit Sensoren schnell und einfach angeschlossen werden können.

![IoT Board](https://gitlab.com/lerntool/m148/-/raw/master/IOT%20Home/IoT%20Board/Foto/IoTBoard.png)
Sie finden die Gerberfiles, um es selbst herzustellen, in diesem Repository
