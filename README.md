# Willkommen im Modul 148!

Hier finden Sie Dokumente für den Unterricht im Modul 148 der Gebäudeinformatiker EFZ. 
Alle Dokumente hier unterstehen folgender Lizenz. 

**Lizenz:**

![](CC-BY-SA.png)

Attribution Share Alike (by-sa): Alles was Sie hier finden darf heruntergeladen, verändert und als Grundlage für eigene Werke verwendet werden. Auch für kommerzielle Zwecke, unter der Bedingung, dass der Urheber des Originals genannt wird und die veränderte Version unter denselben Bedingungen lizenziert und veröffentlicht wird.

Mehr Infos finden Sie [hier](https://creativecommons.org/licenses/by-sa/3.0/ch/)
